<?php

  class Lista{
	private $inicia=0; // colocar 0 o 1 en el parametro del constructor para especificar si lee la primera linea del archivo CSV
	private $num_columnas="";
	private $num_filas="";
	private $tipo_file="";
	private $file="";
	private $lista="";

		public function __construct($file,$tipo_file,$num_filas,$inicia){
			$this->inicia=$inicia;
			$this->tipo_file=$tipo_file;
			$this->file=$file;
			if($inicia==1){$this->num_filas=$num_filas;
				}else{
					$this->num_filas=$num_filas+1;
				} //esta configuracion es para dar escalabilidad al aplicativo 
		}
		
		public function listar(){

		
			$lineas = file($this->file);
			$cont=0; 
			$i=1;
 
			foreach ($lineas as $linea_num => $linea){ 
   
   				if($i >= $this->inicia){ 

       				$datos = explode(",",$linea);
 	   				$this->num_columnas=count($datos);
       				
       				for($x=0 ; $x<$this->num_columnas; $x++){

     					$lista [$cont]= trim($datos[$x]);
						$cont++;
       				}

   				}

   				$i++;

   				if($i>$this->num_filas){
     				break;
   				}
 
   			}
   				
   			$this->lista=$lista;	
		}

		public function ordenar($orden){
			$lista=$this->get_lista();
			$listOrden="";
			$cont=$this->get_numcolumnas();
			$rest=count($lista);
			$cabecera="";
			$pie="";
			$i=0;
			//comprueba si se lee la primera fila del array de usuario
			if($this->inicia==0){
				$cont=$this->get_numcolumnas()*2;
				$i=1;
			}		
			//invierte array de usuario
			 if($orden=="desc"){

			 	for($i=1;$i<$this->get_numFilas(); $i++){

     				for($x=$this->get_numcolumnas();$x>0;$x--){
							$listOrden[$cont-$x]= $lista[$rest-$x];
					}
					$rest-=$this->get_numcolumnas();
					$cont+=$this->get_numcolumnas();
				}
					//
   					for($i=0;$i<$this->get_numcolumnas(); $i++){
   			  			$cabecera[$i]=$lista[$i];
   			  		}
   			  		for($x=0;$x<$this->get_numcolumnas(); $x++){
   			  			$listOrden[$x]=$cabecera[$x];
   			  		}

   			  	$this->lista=$listOrden;
   			}
		}
		
		public function eliminar_fila($indice){
			$lista=$this->get_lista();
			for($x=0 ; $x<$this->num_columnas; $x++){
				array_splice($lista,($indice*$this->num_columnas) ,1);
			}
			$this->num_filas-=1;
			$this->lista=$lista;
			
		}
		public function editar_fila($indice,$datos){
			$lista=$this->get_lista();
			$ubicar=0;
			for($x=0 ; $x<$this->num_columnas; $x++){
				$ubicar=($indice*$this->num_columnas)+$x;
				$lista[$ubicar]=$datos[$x];
			}
			$this->lista=$lista;
		}
		public function mostrarTabla(){
			$filas=$this->num_filas-1;
			$actualiza=json_encode($this->get_lista());
			$panel="<div id='vista' >
						<div id='panel'>
                			<button onclick='editMostrar($filas,$this->num_columnas,".'' .$actualiza.''.");' type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#s-modal'>editar</button>
                			<button onclick='eliminar($filas,$this->num_columnas,".'' .$actualiza.''." );' type='button' class='btn btn-danger' >eliminar</button>
                		</div>
              		<br>";
			$tabla="<table id='tabla' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
			$lista=$this->get_lista();
			$registro="";
			$dato="";
			$html="";
			$head="<thead><tr>";
			$headend="<th>Seleccione</th></tr></thead>";
			$footer="<tfoot><tr>";
			$footerend="<th>Seleccione</th></tr></tfoot><tbody><tr>"; 
    		$cont=0;
    		for($i=0;$i<$this->num_filas;$i++){
      			for($x=0;$x<$this->num_columnas;$x++){

      				if($i==0){//construye head de la tabla y filas
      					
      					$head .="<th>" . $lista[$cont] . "</th>";
      					$footer .= "<th>" . $lista[$cont] . "</th>";
      				}else{
      					
        				$dato="<td id='d$cont'>" . $lista[$cont] . "</td>";
          				$registro=$registro . $dato ;
          			}
          				$cont++;
      			}
      			if($i!=0){
       				$registro=$registro . "<td><input type='checkbox' id='c$i'></td></tr>";
       			}
    		}
    		$head=$head . $headend;
    		$footer=$footer . $footerend;
        	$html=$panel . $tabla . $head . $footer . $registro . "</tbody></table></div>

        	";
        	echo $html;
		}
		public function actualizar_lista($lista,$num_columnas,$numfilas){
			$this->num_columnas=$num_columnas;
			$this->num_filas=$numfilas;
			$this->lista=$lista;
		}

		public function get_numFilas(){
			return $this->num_filas;
		}

		public function get_numcolumnas(){
			return $this->num_columnas;
		}

		public function get_lista(){
			return $this->lista;
		}

	}
?>