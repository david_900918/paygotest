﻿$(document).ready(function() {
    paginar();//se arma la paginacion      
} );

function eliminar(registros,num_columnas,lista){
        
      var eliminacion=[];
      var jObject1={};
      var jObject2={};
      var cont=0;

      var paginacion=0;
      var pag_activa=0;
      var pagina_end=0;
      var id="";
      

          //se calcula tamaño de registros para paginacion si es mayor a diez FILAS
          pag_activa=$('.paginate_button.active>a').last().text(); // ultima paginacion
          paginacion=document.getElementsByName("tabla_length")[0].value; //rango de paginacion
          pagina_end=parseInt(registros/paginacion)+1; //total paginas

          if(registros>=paginacion){
        
            if(pag_activa==pagina_end){
              registros=registros-(paginacion*(parseInt(registros/paginacion)));  
            }else{
              registros=paginacion;
            }
          }
    
           //lista de checkeo 
          for(var i=1 ; i<=registros; i++){
            id=((pag_activa-1)*paginacion)+i;//ids del modal
            var checkboxes = document.getElementById("c"+id).checked;
           
            if(checkboxes==true){
                eliminacion[cont]=id;
                cont++;
            }
          }
    
      if(cont!=0){

          for(x in eliminacion)
            {
              jObject1[x] = eliminacion[x];
            }
            for(x in lista)
            {
              jObject2[x] = lista[x];
            }
    
          var json1 = JSON.stringify(jObject1);//indices a eliminar
          var json2 = JSON.stringify(jObject2);//lista
    
        $.ajax({
         type:"POST",
         data:{data1:json1,data2:json2,data3:num_columnas},
         url: "../controlador/eliminar.php",
         success: function(result){
         if(result!=0){
           $("#vista").html(result);
            bootbox.alert("Eliminacion Exitosa!");
            paginar();
	          }
        }});
      }   
    }
  function editMostrar(registros,num_columnas,lista){

      var edicion=[];// indices a editar
      var datos=[];
      var cont=0;
      var cont2=0;
      var jObject={};
      var jObject1={};
      var jObject2={};
      var paginacion=0;
      var pag_activa=0;
      var pagina_end=0;
      var id="";

          
      //se calcula tamaño de registros para paginacion si es mayor a diez FILAS
      pag_activa=$('.paginate_button.active>a').last().text(); // ultima paginacion
      paginacion=document.getElementsByName("tabla_length")[0].value; //rango de paginacion
      pagina_end=parseInt(registros/paginacion)+1; //total paginas
     
      if(registros>=paginacion){
        
        if(pag_activa==pagina_end){
          registros=registros-(paginacion*(parseInt(registros/paginacion)));  
        }else{
          registros=paginacion;
        }
      }
      //lista de checkeo y datos a editar
      for(var i=1 ; i<=registros; i++){
        
        id=((pag_activa-1)*paginacion)+i;//ids del modal
                  
        var checkboxes = document.getElementById("c"+id).checked;
        if(checkboxes==true){
            edicion[cont]=id;
            cont++;
            
            for(var y=0 ; y<num_columnas; y++) {
              jObject2[cont2]= lista[(id*num_columnas)+y];
              cont2++;
            }
        }
      }
      //envio de datos
      for(x in edicion){
          jObject1[x] = edicion[x];
        }
      for(x in lista){
          jObject[x] = lista[x];
        }
       
      var json = JSON.stringify(jObject);
      var json1 = JSON.stringify(jObject1);
      var json2 = JSON.stringify(jObject2);
      
     $.ajax({
      type:"POST",
      data:{data:json,data1:json1,data2:json2,data3:num_columnas},
      url: "../controlador/editMostrar.php",
      success: function(result){
      if(result!=0){  
        $("#tabla-edit").html(result);
      }
    }});
  }

  function editar(indice_fila,lista,num_columnas){
      var jObject={};
      var jObject1={};
      var jObject2={};
      var datos=[];
      var cont=0;
      var id="";

      if(indice_fila!=0){//verifica que existan selecciones

      for(x in indice_fila){
          jObject[x] = indice_fila[x];
          //captura de los campos nuevos
          for(var y=0;y<num_columnas;y++){
            id=((x*num_columnas)+y);
            datos[cont]=document.getElementById("de"+id).value;
            cont++;
          }//fin captura
        }
      for(x in lista){
          jObject1[x] = lista[x];
        }
      for(x in datos){
          jObject2[x] = datos[x];
        }

      var json = JSON.stringify(jObject);//indices a editar
      var json1 = JSON.stringify(jObject1);//lista de datos
      var json2 = JSON.stringify(jObject2);//datos nuevos
     
      console.log(json2);
      
     $.ajax({
      type:"POST",
      data:{data:json,data1:json1,data2:json2,data3:num_columnas},
      url: "../controlador/editar.php",
      success: function(result){
      if(result!=0){  
        $("#vista").html(result);
        paginar();
	      bootbox.alert("Edicion Exitosa!");
      } 
    }});
    }
  }

  function paginar(){
    $('#tabla').DataTable({
        "ordering": false,
        stateSave: true,
        searching: false,
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
 
      }); 
  }