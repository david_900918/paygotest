<!DOCTYPE html>
	<html lang="es">

	<head>
		<title>APP - SUBIR CSV</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap.min.css"/></link>
		<link rel="stylesheet" href="css/custom.css"/></link>
		<script type="text/javascript" href="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<?php
		 	session_start();
      	 	$_SESSION['user']="user";
	
			if(isset($_GET["mensaje"])){
				$mensaje="";
				$mensaje=$_GET["mensaje"];
				echo $mensaje ;
			}else{
				$mensaje="";
			}
		?>
		
		<form class="formulario" action="lista" enctype="multipart/form-data" method="post">
			<h1 class="titulo">EDITOR-CSV</h1>
   			<input class="form-control" id="archivo" accept=".csv,application/vnd.ms-excel" name="archivo" type="file"  title="Subir csv" required/>
   			<input class="form-control" type="number" min="1" max="1000" id="numRegistros" name="numRegistros" placeholder="Número de registros" title="Numero de registros a visualizar" required/>
   			<select class="form-control" name="orden" id="orden">
   				<option selected value="asc">Orden</option>
   				<option value="asc">Ascendente</option>
   				<option value="desc" >Descendente</option>
   			</select>
			<input class="btn btn-primary" id="enviar" name="enviar" type="submit" value="Procesar" />
		</form>
		
		<p><?php echo $mensaje; ?></p>
	</body>
<html>