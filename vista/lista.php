<!DOCTYPE html>
	<html lang="es">

	<head>
		<title>APP - Lista CSV</title>
		<meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
    
    <link rel="icon" href="icono.gif" type="image/gif" sizes="16x16"></link>
    <link rel="stylesheet" href="css/bootstrap.min.css"/></link>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"/></link>
    <link rel="stylesheet" href="css/custom.css"/></link>

		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
   
  </head>
	<body>

  <header>
  </header>
  
  <section>     
	 <?php
      session_start();
      if(isset($_SESSION["user"])){
          require_once("../controlador/import.php");//Genera tabla
          session_destroy();
        }else{ 
          session_destroy();
          header("Location:../index.php");
        }
      ?>
    <!-- Modal -->
    <div id="s-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Editar Datos</h4>
          </div>
          <div class="modal-body">
            <div id="tabla-edit">
            </div>
          </div>
        </div>
      </div>
    </div>
  <section>

	</body>
</html>