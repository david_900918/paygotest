<?php

require_once("../modelo/lista.php");

if(isset($_POST['data']) && isset($_POST['data1']) && isset($_POST['data2'])){
	$checkeo=null;
	$miLista=null;
	$num_columnas=0;
	
	/// Obtenemos el json enviados
	$indices_edit=json_decode($_POST['data'], true);
	$lista=json_decode($_POST['data1'], true);
	$datos=json_decode($_POST['data2'], true);
	$num_columnas=json_decode($_POST['data3'], true);
	$num_filas=count($lista)/$num_columnas;
	$datos_fila=[];

	//Actualizamos tabla
	$miLista=new Lista(null,null,null,0);
	$miLista->actualizar_lista($lista,$num_columnas,$num_filas);

	//eliminamos registros
	for($i=0;$i<count($indices_edit);$i++){

		for($x=0;$x<$num_columnas;$x++){//agrupa una fila de datos para ser editados en la lista principal
			$datos_fila[$x]=$datos[($i*$num_columnas)+$x];

		}
		$miLista->editar_fila($indices_edit[$i],$datos_fila);
	}	
	$miLista->mostrarTabla();

 }
?>