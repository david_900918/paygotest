<?php

require_once("../modelo/lista.php");

if(isset($_POST['data1']) && isset($_POST['data2']) && isset($_POST['data3'])){
	$checkeo=null;
	$miLista=null;
	$num_columnas=0;
	
	/// Obtenemos el json enviados
	$checkeo=json_decode($_POST['data1'], true);
	$miLista=json_decode($_POST['data2'], true);
	$num_columnas=json_decode($_POST['data3'], true);
	$num_filas=count($miLista)/$num_columnas;

	//Actualizamos tabla
	$lista=new Lista(null,null,null,0);
	$lista->actualizar_lista($miLista,$num_columnas,$num_filas);
	
	//eliminamos registros
	for($i=0;$i<count($checkeo);$i++){
		$lista->eliminar_fila($checkeo[$i]-$i);
	}	
	$lista->mostrarTabla();
}
?>