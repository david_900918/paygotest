<?php

require_once("../modelo/lista.php");
//obtenemos el archivo .csv
$tipo = $_FILES['archivo']['type'];
$archivotmp = $_FILES['archivo']['tmp_name'];
$numRegistros=$_POST['numRegistros'];
$orden=$_POST['orden'];

try{

  if($tipo==".csv" || $tipo=="application/vnd.ms-excel"){

      $lista=new Lista($archivotmp,$tipo,$numRegistros,0);// se iniciara a leer en CSV desde la primera linea de esta manera la tabla sera escalable segun los campos que tenga el CSV 
      $lista->listar();
      $lista->ordenar($orden);
      
      $num_filas=$lista->get_numFilas();
      $num_columnas=$lista->get_numcolumnas();
      $miLista=$lista->get_lista();
      $lista->mostrarTabla();
     
    }else{
      header("Location:../index.php?mensaje=El archivo no corresponde a un csv");
    }
                      
  }catch(Exception $e){
    echo "hola";
  }
?>